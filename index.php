<!DOCTYPE html>
<html>
  <head>
    <title>Dynamic Accummulator Weeds</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style type="text/css" title="currentStyle">
      @import "css/demo_page.css";
      @import "css/demo_table.css";
      .center {
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>Dynamic Accumulators of Nutrients for Composting</h1>
      <div class="row">
        &nbsp;
      </div>
      
      <div class="row">

        <table class="display" id="example" width="100%" border="1">
          <thead>
            <tr><th>Name</th>
              <th>Botanical Name&#9;</th>
              <th>Na</th>
              <th>I</th>
              <th>Fl</th>
              <th>B</th>
              <th>Si</th>
              <th>S</th>
              <th>N</th>
              <th>Mg</th>
              <th>Ca</th>
              <th>K</th>
              <th>P</th>
              <th>Mn</th>
              <th>Fe</th>
              <th>Cu</th>
              <th>Co</th>
            </tr>
          </thead>
          <tbody>
            <tr><td>Alfalfa</td>
              <td>Medicago sativa</td>
              <td></td>
              <td></td>
              <td></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Arrowroot</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Bladder wrack</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Borage</td>
              <td>Borago officinalis</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Bracken, eastern</td>
              <td>Pteridium aquifolium</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td class="center">x</td>
            </tr>
            <tr><td>Bridal bower&#9;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Buckwheat</td>
              <td>Fagopyrum esculentums</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Burdock</td>
              <td>Arctium minus</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Calamus</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Carageen</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Caraway</td>
              <td>Carum carvi</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Carrot leaves</td>
              <td>Daucus carota</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Cattail</td>
              <td>Typha latifolia</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Chamomile, corn</td>
              <td>Anthemis arvensis</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Chamomile, German</td>
              <td>Chamomilla recutita</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr><td>Chickweed</td>
              <td>Stellaria media</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td class="center">x</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          <tr> 
            <td>Chicory</td>
            <td>Cichorium intybus</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Chives</td>
            <td>Allium sp.</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Cleavers</td>
            <td>Galium aparine</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Clovers</td>
            <td>Trifolium sp.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Clover, hop</td>
            <td>Medicago lupulina</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Clover, rabbit foot</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Clover, red</td>
            <td>Trifolium protense</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Clover, white</td>
            <td>Trifolium repens</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Coltsfoot</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Comfrey</td>
            <td>Symphytum officinale</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Dandelion</td>
            <td>Taraxacum vulgare</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Dock, broad leaved&#9;</td>
            <td>Rumex obtusifolias</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Dulse</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Fat hen</td>
            <td>Atriplex hastata</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Fennel</td>
            <td>Foeniculum vulgare</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Flax, seed</td>
            <td>Linum usitatissimum</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Garlic</td>
            <td>Allium sativum</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Groundsel</td>
            <td>Senecio vulgaris</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Horsetails</td>
            <td>Equisetum sp.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
          </tr>
          <tr> 
            <td>Kelp</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Lamb’s quarters</td>
            <td>Chenopodsum album</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Lemon Balm</td>
            <td>Melissa offcinalis</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Lupine</td>
            <td>Lupinus sp.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Marigold, flowers</td>
            <td>Tagetes sp.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Meadow sweet</td>
            <td>Astilbe sp.</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Mistletoe</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Mullein, common</td>
            <td>Verbascum sp.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Mustards</td>
            <td>Brassica sp.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Nettles, stinging</td>
            <td>Urtica urens</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>Oak, bark</td>
            <td>Quercus sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Oat Straw</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Parsley</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Peppermint</td>
            <td>Mentha piperita</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Pigweed, red root</td>
            <td>Amaranthus retroflexus .</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Plantains</td>
            <td>Plantago sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Primrose</td>
            <td>Oenothera biennis</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Purslane</td>
            <td>Portulaca oleracea</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Salad burnet</td>
            <td>Poterium sanguisorba</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Savory</td>
            <td>Satureja sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Scarlet Pimpernel</td>
            <td>Anagallis arvensis</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Shepherd’s purse</td>
            <td>Capsella bursa-pastoris</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Skunk cabbage</td>
            <td>Navarretia squanosa</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Sorrel, sheep</td>
            <td>Rumex acetosella</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Sow thistle</td>
            <td>Sonchus arvensis</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
          </tr>
          <tr> 
            <td>Spurges</td>
            <td>Euphorbia sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Strawberry, leaves</td>
            <td>Fragaria sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Tansy</td>
            <td>Tanacetum vulgare</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Thistle, Canada</td>
            <td>Cirsium arvense</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Thistle, creeping</td>
            <td>Sonchus arvensis</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Thistle, nodding</td>
            <td>Carduus nutans</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Thistle, Russian</td>
            <td>Salsola pestifer</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Toadflax</td>
            <td>Linaria vulgaris</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Tobacco, stems/stalk</td>
            <td>Nicotiana sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Valerian</td>
            <td>Valeriana ofjicinalis</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Vetches</td>
            <td>Vicia sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
          </tr>
          <tr> 
            <td>Watercress</td>
            <td>Nasturtium ofpcinale</td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Willow, bark</td>
            <td>Salix sp.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr> 
            <td>Yarrow</td>
            <td>Achilea millefolium</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td class="center">x</td>
            <td></td>
            <td></td>
            <td class="center">x</td>
            <td></td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- JavaScript plugins (requires jQuery) -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf-8">
      $(document).ready(function() {
        $('#example').dataTable( {
          'iDisplayLength': 100
        } );
      } );
    </script>
    <!-- Optionally enable responsive features in IE8 -->
    <script src="js/respond.js"></script>
  </body>
</html>